# 介绍

IMI 是基于 Swoole 开发的协程 PHP 开发框架，拥有常驻内存、协程异步非阻塞IO等优点。

IMI 框架文档丰富，上手容易，致力于让开发者跟使用传统 MVC 框架一样顺手。

IMI 框架底层开发使用了强类型，易维护，性能更强。支持 Aop ，支持使用注解和配置文件注入，完全遵守 PSR-3、4、7、11、15、16 标准规范。

框架的扩展性强，开发者可以根据实际需求，自行开发相关驱动进行扩展。不止于框架本身提供的功能和组件！

我们认为一个框架不仅需要代码健壮、简单易用，文档也是十分重要，所以文档的完善是我们工作的重点，相信一个好的文档能够让你事半功倍！

欢迎有志之士加入我们，一起开发完善！技术好的贡献代码，文笔好的贡献文档，有好的想法也欢迎与我们交流！

> 框架暂未实战验证，请无能力阅读和修改源代码的开发者，暂时不要用于实际项目开发，等待我们的实战检验完善，我们不希望因此为您造成不便！

同时欢迎有志之士加入我们，一起开发完善！

QQ群：74401592 [![点击加群](https://pub.idqqimg.com/wpa/images/group.png "点击加群")](https://shang.qq.com/wpa/qunwpa?idkey=e2e6b49e9a648aae5285b3aba155d59107bb66fde02e229e078bd7359cac8ac3)，如有问题会有人解答和修复。

### 功能组件

- [x] Aop (注解 / 配置文件)
- [x] Container (PSR-11)
- [x] 注解
- [x] 全局事件/类事件
- [x] HttpServer
- [x] HttpRequest/HttpResponse (PSR-7)
- [x] Http 中间件、注解路由、配置文件路由 (PSR-15)
- [x] Session (File + Redis)
- [x] View (html + json + xml)
- [x] 日志 (PSR-3 / File + Console)
- [x] 缓存 (PSR-16 / File + Redis)
- [x] Redis 连接池
- [x] 协程 MySQL 连接池
- [x] PDO 连接池
- [ ] 协程 PostgreSQL 连接池
- [x] Db 连贯操作
- [x] 关系型数据库 模型 ORM
- [x] 跨进程共享内存表 模型 ORM
- [x] Task 异步任务
- [x] 命令行开发辅助工具
- [ ] 图形化管理工具
- [x] 业务代码热更新
- [ ] RPC 远程调用
- [ ] WebSocket 服务器相关……
- [ ] TCP 服务器相关……

> 日志、缓存都支持：多驱动 + 多实例 + 统一操作入口
> 
> 所有连接池都支持：同步 + 异步 + 多驱动 + 多实例
