# Summary

## 基础入门

* [序言](README.md)
* [介绍](base/intro.md)
* [环境要求](base/env.md)
* [开始一个新项目](base/new.md)
* [配置文件](base/config.md)

## 框架核心

* [生命周期](core/lifeCycle.md)
* [容器](core/container.md)
* [事件](core/events.md)
* [中间件](core/middleware.md)
* [进程名称管理](core/processNameManager.md)

## 功能组件

* [配置读写](components/config/index.md)
* [服务器](components/httpserver/fu-wu-qi.md)
  * [HTTP 服务器](components/httpserver/index.md)
    * [路由](components/httpserver/route.md)
    * [中间件](components/httpserver/middleware.md)
    * [控制器](components/httpserver/controller.md)
    * [RESTful](components/httpserver/restful.md)
    * [Session](components/httpserver/session.md)
    * [视图](components/httpserver/view.md)
    * [错误异常处理](components/httpserver/error.md)
    * [404处理](components/httpserver/404.md)
    * [HTTP 验证器](components/httpserver/validation.md)
  * [WebSocket 服务器](components/websocketServer/index.md)
    * [HTTP 路由](components/websocketServer/httpRoute.md)
    * [HTTP 控制器](components/websocketServer/httpController.md)
    * [WebSocket 控制器](components/websocketServer/websocketController.md)
    * [中间件](components/websocketServer/middleware.md)
    * [会话数据](components/websocketServer/session.md)
    * [连接分组](components/websocketServer/group.md)
    * [不使用中间件](components/websocketServer/noMiddleware.md)
  * [TCP 服务器](components/tcpServer/index.md)
    * [TCP 控制器](components/tcpServer/controller.md)
    * [中间件](components/tcpServer/middleware.md)
    * [会话数据](components/tcpServer/session.md)
    * [连接分组](components/tcpServer/group.md)
    * [不使用中间件](components/tcpServer/noMiddleware.md)
  * [UDP 服务器](components/udpServer/index.md)
    * [UDP 控制器](components/udpServer/controller.md)
    * [中间件](components/udpServer/middleware.md)
    * [不使用中间件](components/udpServer/noMiddleware.md)
* [连接池](components/pool/index.md)
* [ORM](components/orm/index.md)
* [数据库模型](components/orm/RDModel.md)
  * [模型事件](components/orm/RDModel/event.md)
  * [模型关联](components/orm/relation.md)
    * [介绍](components/orm/relation/index.md)
    * [一对一关联](components/orm/relation/oneToOne.md)
    * [一对多关联](components/orm/relation/oneToMany.md)
    * [多对多关联](components/orm/relation/manyToMany.md)
    * [多态一对一关联](components/orm/relation/polymorphicOneToOne.md)
    * [多态一对多关联](components/orm/relation/polymorphicOneToMany.md)
    * [多态多对多关联](components/orm/relation/polymorphicManyToMany.md)
* [内存表模型](components/orm/MemoryTableModel.md)
* [Redis 模型](components/orm/RedisModel.md)
* [数据库](components/db/index.md)
* [Redis](components/redis/index.md)
* [缓存](components/cache/index.md)
* [日志](components/log/index.md)
* [验证器](components/validation/index.md)
* [锁](components/lock/index.md)
* [事件监听](components/event/index.md)
* [后台任务](components/task/index.md)
* [AOP](components/aop/index.md)
* [进程](components/process/index.md)
* [进程池](components/process-pool/index.md)
* [热更新](components/hotupdate/index.md)
* [Phar 支持](components/phar/index.md)
* [数据结构](components/struct/index.md)
  * [Atomic](components/struct/atomic.md)
  * [Channel](components/struct/channel.md)
  * [协程 Channel](components/struct/co-channel.md)
  * [MemoryTable](components/struct/memory-table.md)
  * [枚举](components/struct/enum.md)
  * [LazyArrayObject](components/struct/LazyArrayObject.md)
  * [ArrayList](components/struct/ArrayList.md)
  * [FilterableList](components/struct/FilterableList.md)

## 工具类

* [全局函数](utils/functions.md)
* [Imi](utils/Imi.md)
* [Args](utils/Args.md)
* [ArrayUtil](utils/ArrayUtil.md)
* [Bit](utils/Bit.md)
* [ClassObject](utils/ClassObject.md)
* [Coroutine](utils/Coroutine.md)
* [File](utils/File.md)
* [ObjectArrayHelper](utils/ObjectArrayHelper.md)
* [Random](utils/Random.md)
* [Swoole](utils/Swoole.md)
* [Text](utils/Text.md)
* [Pagination](utils/Pagination.md)

## 注解

* [注入值注解](annotations/injectValue.md)
* [方法参数过滤器](annotations/filterArg.md)

## 开发者工具

* [介绍](dev/intro.md)
* [服务器工具](dev/server.md)
* [生成工具](dev/generate.md)
  * [模型生成](dev/generate/model.md)
  * [控制器生成](dev/generate/controller.md)
* [进程工具](dev/process.md)

## 进阶开发

* [性能优化](adv/performance.md)
* [参与框架开发](adv/devp.md)

