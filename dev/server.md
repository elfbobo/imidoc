# 服务器工具

## 开启服务

无参数

示例：

```
HttpDemo/bin/imi server/start
```

## 停止服务

无参数

示例：

```
HttpDemo/bin/imi server/stop
```

## 重新加载服务

可以让项目文件更改生效

无参数

示例：

```
HttpDemo/bin/imi server/reload
```
