# 序言

发布本资料须遵守开放出版许可协议 1.0 或者更新版本。

未经版权所有者明确授权，禁止发行本文档及其被实质上修改的版本。未经版权所有者事先授权，禁止将此作品及其衍生作品以标准（纸质）书籍形式发行。

如果有兴趣再发行或再版本手册的全部或部分内容，不论修改过与否，或者有任何问题，请联系版权所有者

[admin@yurunsoft.com](mailto:admin@yurunsoft.com)

本文档的版权归`IMI开发团队`所有，本文档及其描述的内容受有关法律的版权保护，对本文档内容的任何形式的非法复制，泄露或散布，将导致相应的法律责任。

## 本文档协作地址

码云 Gitee：https://gitee.com/yurunsoft/imidoc

Github：https://github.com/Yurunsoft/imidoc
